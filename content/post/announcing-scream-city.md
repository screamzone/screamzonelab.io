---
title: "Announcing SCREAM CITY"
date: 2019-03-16T18:22:58+11:00
Blurb: "SCREAM all through the CITY"
Tags: ["game-jam", "scream-city"]
Categories: ["post"]

---

After the very successful SCREAMWORLD, we return with our newest jam: **SCREAM CITY**!

![SCREAM CITY logo](/jams/scream-city/logo.png)

Yes my friends, prepare to dive into a city of fear. You may now enter the jam over on [itch.io](https://itch.io/jam/scream-city) and find a team on [Crowdforge](https://crowdforge.io/jams/screamcity). As always, our [Discord](https://discordapp.com/invite/vMVysVJ) is open for new members to join!

See you on April 12th at 10:00 AM AEST!