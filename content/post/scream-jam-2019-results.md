---
title: "SCREAM JAM 2019 Results!"
date: 2019-11-13T00:00:00+10:00
Blurb: "The results for SCREAM JAM 2019 are finally out!"
Tags: ["game-jam", "scream-jam-2019"]
Categories: ["post"]
---

With 73 entries (at time of voting end) and 473 ratings, SCREAM JAM 2019 was our largest game jam so far! It's been an amazing experience building a community since SCREAM JAM 2018. We've seen tons of amazing games get created through our jams and we're excited to see what comes next!

Firstly, I'd like to thank [naver](https://jate8d.itch.io/)! She joined the Discord moderation team at the start of the jam and immediately integrated themselves as an invaluable part of the team. Some ways in which she helped were by taking care of jammers who needed help, helping us to clarify the jam rules, running many post-jam Twitch streams, and even creating an emoji set for use on the Discord. Without her, the Discord would've been a much more boring place during this jam!

<hr />

This was the first time we introduced a sponsorship; [Satoshi's Games](https://satoshis.games/) sponsored a 100€ prize for the highest rated game which was uploaded to their [Elixir](https://elixir.app/) platform. This prize was won by DataGhost for their game "Remnant". "Remnant" also won the SCARIEST GAME award!
{{<itch id="502151">}}

Another first: this was the first time that the first place prize for BEST GAME (GENERAL) was won by three entries! Along with "Remnant", this prize was won by Scullysaurus' terrifying workplace nightmare "Aamon Inc" and mikorashu's atmospheric and immersive "Wooden Hearts".
{{<itch id="502641">}}
{{<itch id="502757">}}

Through an error on our part, the BEST GAME (HOST'S CHOICE) category was left open to the public instead of closed to hosts only. Whoops! But the people voted, and I can only guess they voted for what they thought the hosts would like most? That in mind, it seems that people feel we'd like Itizso's minimal and strategic "Grim Tactics" the most!
{{<itch id="501941">}}

<hr />

So that's that! We had an incredible turnout with many quality games coming through. Thank you to everyone who joined, everyone who submitted games, and everyone who had to cancel games but tried their best. I'm looking forward to our next jam, [SCREAM SOLSTICE](https://itch.io/jam/scream-solstice) in December!