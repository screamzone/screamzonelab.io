---
title: "SCREAM JAM 2019 Sponsorship: Satoshis Games"
date: 2019-10-11T08:30:00+11:00
Blurb: "Our first sponsor!"
Tags: ["game-jam", "scream-jam-2019", "sponsorship"]
Categories: ["post"]

---

Hi everyone! SCREAM JAM 2019 is almost upon us, and I'm very keen to see what games everyone comes up with. We've been waiting for this for months now and it's amazing to see so many people join and be excited for it!

And it's only fitting that our (by FAR) biggest game jam so far should have a prize! An hour and a half before the jam starts, it's time to announce our first sponsor ever!

![Satoshis Games logo](/sponsors/satoshis-games/logo.png)

Satoshis Games have kindly agreed to sponsor a prize of 100€ to the highest rated game in the BEST GAME (GENERAL) category which has been uploaded on Elixir (https://elixir.app/) by the end of SCREAM JAM 2019. This prize can be awarded through PayPal, a bank transfer, or via a Bitcoin transfer (whichever the winner prefers).

![Elixir logo](/sponsors/satoshis-games/elixir.svg)

Elixir is a new gaming platform by Satoshis Games where indie developers can distribute their games (similarly to itch.io). Developers keep full ownership of their games.

How do you upload your game to Elixir? Read this article: https://medium.com/@federicospitaleri/how-to-upload-a-game-on-elixir-5dda49898e42?sk=74ed653bb18cb06be050196413ef68a2

Games that have been uploaded to Elixir during the jam will also have the chance to win an additional prize. After SCREAM JAM 2019 ends, developers will have 15 days to integrate Bitcoin micro-transactions through Satoshis Games API (APItoshi: https://whiteyhat.github.io/apitoshi-docs/). The best game to be integrated with this API (as judged by Satoshis Games) will win a prize of 400€ (or the equivalent in Bitcoin, if so chosen).

At the end of the SCREAM JAM 2019, Satoshis Games will provide examples of Bitcoin integrated games (eg. a Super Mario style game where every coin is worth real Bitcoin: https://medium.com/satoshis-games/how-to-earn-bitcoin-while-playing-super-bro-969a7823a4a8) and tutorials about how to integrate the API.

For the moment, just focus on building a great game for SCREAM JAM 2019 and upload it on Elixir!