---
title: "Welcome to the Blog"
date: 2019-03-16T18:03:26+11:00
draft: true
Blurb: "It's been a long time coming, but here we are. SCREAM ZONE is open for business!!!"
Tags: []
Categories: ["post"]

---

Hi!

It's been a long time coming, but here we are. ***SCREAM ZONE*** is open for business!!!

Here is what we aim for SCREAM ZONE to be:

- An archive of every game jam we have hosted, including the top winners
- A blog showcasing the ongoing efforts of SCREAM ZONE members