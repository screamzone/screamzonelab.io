---
title: "One Year of SCREAM ZONE Game Jams"
date: 2019-10-06T4:00:00+11:00
Blurb: "A recap of every game jam we've run so far."
Tags: ["game-jam", "scream-jam", "scream-season", "screamworld", "scream-city", "scream-empire"]
Categories: ["post"]

---

With [SCREAM JAM 2019](https://itch.io/jam/scream-jam-2019) looming, what better time is there to look back at some of the previous SCREAM JAMS and how we got through a year of scares?

<hr />

# Background

SCREAM CATALOGUE originated from a joke twitter account that would do nothing but post inane screams. However, we've been active as CAVEWARE DIGITAL and participated in countless game jams such as the Ludum Dare over the past five years. One day we were talking and decided to create our own jam as a joke, and things truly started to spiral from there...

<hr />

# SCREAM JAM (October 2018)

The original SCREAM JAM was a humorous celebration of the spooky, the scary, and all things in between. It was a general horror-themed Halloween jam that tested the waters for hosting jams in the future. Given the jam yielded 23 submissions, we were confident to say that the jam was a huge success!

The games spanned many genres. Some of our favourites included *The Emulator*, a very polished survival horror in which the player is chased around a house by a bloodied madman. Not all were tonally similar – *Pumpkin Pete* involves the player traversing a toylike world as a tiny skeleton! 

The winner of SCREAM JAM was...

{{<itch id="318280">}}

<hr />

# SCREAM SEASON (December 2018)

Continuing the original idea to have a horror jam for every festive season, we decided to create a Christmas-themed horror jam. SCREAM SEASON also introduced the idea of having a jam theme, which was chill – fitting for the season! In the end there were 18 frosty games that were submitted for the jam!

Some of our favourites were *Cold Wick*, which was a campfire horror shooter that featured lots of creepy and adorable monsters. Games like *Three Mile* started the tradition of having interactive fiction entries be a mainstay of the jam, as well as humorous games, like *Thirstiest Time of the Year*!

The winner of SCREAM SEASON was...

{{<itch id="342876">}}

<hr />

# SCREAMWORLD (February 2019)

SCREAMWORLD was the first horror jam based around a theme rather than a festive season, as we decided to run a SCREAM JAM every two months. It was our largest yet at 26 submissions and 100 entrants, which motivated us to keep going! The theme of space similarly took the jam to new frontiers!   

With so many games to play, finding our favourites was a challenge! In the end, *Affection* impressed us as a very well-paced and eerie adventure game. The game *Sheila and the 7 Gates to Hell* was another eerie first-person narrative experience that similarly took some strange and surreal directions. 

The winner of SCREAMWORLD was...

{{<itch id="379416">}}

<hr />

# SCREAM CITY (April 2019)

SCREAM CITY was the first jam made after the creation of the SCREAM ZONE. SCREAM CITY had an ambitious idea of adding an additional theme on top of the city that recognised entries that featured a real-world city in the game’s setting. The quality of the 14 games was high for this one!

Our favourites included *Scotch Broom*, a truly surreal adventure game involving murder, intrigue, and wounds that talk, and *Obsession*, a twisted psychological horror about inner demons. Some games approached the jam from interesting genres, such as the creepy exploration game *Ill Omen*.

The winner of SCREAM CITY was...

{{<itch id="404668">}}

<hr />

# SCREAM EMPIRE (July 2019)

After a hiatus, SCREAM JAM returned with a more abstract theme – *empire* – that proved to be interesting and rewarding! This SCREAM JAM saw more diversity and originality than usual as a result. In the process of playing the 17 games we were taken to tombs, ventilation ducts, deserts and forests!

Our favourites included *Singing Valley*, a point and click adventure involving the investigation of a cult in the woods, and *The Fallen Empress*, a sort of deduction game learning about the ghosts in an ancient Nordic tomb. *Whispers from the Sky* was also a fun adventure with unique and spooky characters.

The winner of SCREAM EMPIRE was...

{{<itch id="457355">}}

<hr />

Looking back on a year of SCREAM JAMS, here at SCREAM ZONE we'd like to thank all the folks that have joined, submitted and rated games, as well as those that have expressed interest in the jam. Whether creating winning entries, first attempts or works in progress, it's been great playing all your spooky games, so thank you for making the SCREAM JAM experience a blast! 

Hope to see you in a few days for [SCREAM JAM 2019](https://itch.io/jam/scream-jam-2019)!