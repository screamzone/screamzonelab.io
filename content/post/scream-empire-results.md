---
title: "SCREAM EMPIRE Results!"
date: 2019-08-09T13:10:00+11:00
Blurb: "The results for SCREAM EMPIRE are finally out!"
Tags: ["game-jam", "scream-empire"]
Categories: ["post"]

---

Voting is over for our fifth game jam and the results are finally out! This was one of our more active game jams, and we're hoping for the next one to be even bigger!

<hr />

Congratulations to CapsuleMaglev for winning BEST GAME with Singing Valley, a very interesting point and click game that I’m excited to see the future of!
{{<itch id="457355">}}

Congratulations to MagicCat Games for winning SCARIEST GAME with Ventcrawler, a game that made me actually scream when we did Scream Stream, so I’m not surprised it won!
{{<itch id="457388">}}

And congratulations to Gaia Fiorenza for winning BEST THEME with Citizen, a brilliant short experience that kept me gripped for its short play time. Again, I look forward to its future!
{{<itch id="457336">}}

Apart from Singing Valley, two games won Host’s Choice!

The beautifully illustrated and told Whispers From The Sky by Portrait Prophecies.
{{<itch id="451954">}}

And the imaginative and mysterious The Fallen Empire by Elena Tchijakoff!
{{<itch id="457241">}}

<hr />

Thank you to everyone who joined, everyone who submitted games, and everyone who had to cancel games but tried their best. It was great having you all along for the ride and we hope the you’ll join us again in October for [SCREAM JAM 2019](https://itch.io/jam/scream-jam-2019)!