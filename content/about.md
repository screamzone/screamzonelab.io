---
title: "About"
description: "about this blog"
date: "2018-02-11"
Weight: 3
---

<div class="about-page">
  <div class="post-title">What is SCREAM ZONE?</div>
  <div class="post-content about">
  <br />
    <img alt="Most members of Caveware Digital" src="/about/team.png" />
    <div class="caption">Josef and Ryan run SCREAM ZONE</div>
  <br />
    SCREAM ZONE was created in late 2018 as a Discord server to house our first game jam, <a href="https://itch.io/jam/screamjam" target="_blank">SCREAM JAM</a>. Since then, we have run bi-monthly jams and have grown a community of wonderful game developers. This website exists as a place to host information about past jams and showcase the efforts of our participants through the blog. It also acts as the home for SCREAM CATALOGUE, which may choose to host any content here.
  </div>
  <br />
  <div class="post-title">What is SCREAM CATALOGUE?</div>
  <br />
  <div class="post-content about">
    SCREAM CATALOGUE is a horror / comedy game development crew and music label headed by Ryan Liddle, with very occasional appearances under the title by Josef Frank. Originally gaining a small following as an extreme parody of vaporwave and its small record label tropes, SCREAM CATALOGUE has somewhat of a rebrand when the first SCREAM JAM was theorised by Josef and Ryan.
  </div>
  <br />
  <div class="post-title">What is Caveware Digital?</div>
  <br />
  <div class="post-content about">
    Caveware Digital is a collective of creatives from Newcastle, NSW, Australia. While at one point considered to be a pathway to commercial success, Caveware now exists as a catch-all showcase for our various projects and as a symbol that links them in an aesthetic way. Both Josef and Ryan are members of Caveware, but Josef is the primary user of the moniker.
  </div>
</div>